package edu.ntnu.oflarsen.idatt2001.mappedel1.hospital;
import edu.ntnu.oflarsen.idatt2001.mappedel1.hospital.healthpersonal.Nurse;
import edu.ntnu.oflarsen.idatt2001.mappedel1.hospital.healthpersonal.doctor.GeneralPractitioner;
import edu.ntnu.oflarsen.idatt2001.mappedel1.hospital.healthpersonal.doctor.Surgeon;

/**
 * HospitalTestData class
 * Pre made class given to the students by the teachers
 */
public final class HospitalTestData {

    private HospitalTestData() {
        // not called
    }

    /**
     * Method to fill a Hospital with test data
     * @param hospital
     */
    public static void fillRegisterWithTestData(final Hospital hospital) {
        //The way the test data is added is pretty bad in my opinion. It uses the fact that we use a bad type aggregation
        // They add the test data directly to the list, without using any methods, therefore they can add two people with the
        // same name and number, because there is no method to deny this, because it just gets added to the list...
        // Add some departments
        Department emergency = new Department("Akutten");
        emergency.getEmployees().add(new Employee("Odd Even", "Primtallet", ""));
        emergency.getEmployees().add(new Employee("Huppasahn", "DelFinito", ""));
        emergency.getEmployees().add(new Employee("Rigmor", "Mortis", ""));
        //Same person with two position
        emergency.getEmployees().add(new GeneralPractitioner("Inco", "Gnito", ""));
        emergency.getEmployees().add(new Surgeon("Inco", "Gnito", ""));
        emergency.getEmployees().add(new Nurse("Nina", "Teknologi", ""));
        emergency.getEmployees().add(new Nurse("Ove", "Ralt", ""));
        emergency.getPatients().add(new Patient("Inga", "Lykke", ""));
        emergency.getPatients().add(new Patient("Ulrik", "Smål", ""));
        hospital.getDepartments().put(emergency.getDepartmentName(), emergency);
        Department childrenPolyclinic = new Department("Barn poliklinikk");
        childrenPolyclinic.getEmployees().add(new Employee("Salti", "Kaffen", ""));
        childrenPolyclinic.getEmployees().add(new Employee("Nidel V.", "Elvefølger", ""));
        childrenPolyclinic.getEmployees().add(new Employee("Anton", "Nym", ""));
        childrenPolyclinic.getEmployees().add(new GeneralPractitioner("Gene", "Sis", ""));
        childrenPolyclinic.getEmployees().add(new Surgeon("Nanna", "Na", ""));
        childrenPolyclinic.getEmployees().add(new Nurse("Nora", "Toriet", ""));
        childrenPolyclinic.getPatients().add(new Patient("Hans", "Omvar", ""));
        childrenPolyclinic.getPatients().add(new Patient("Laila", "La", ""));
        childrenPolyclinic.getPatients().add(new Patient("Jøran", "Drebli", ""));

        hospital.getDepartments().put(childrenPolyclinic.getDepartmentName(), childrenPolyclinic);
    }
}
