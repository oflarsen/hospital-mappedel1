package edu.ntnu.oflarsen.idatt2001.mappedel1.hospital.healthpersonal.doctor;

import edu.ntnu.oflarsen.idatt2001.mappedel1.hospital.Patient;
import edu.ntnu.oflarsen.idatt2001.mappedel1.hospital.healthpersonal.Doctor;

/**
 * Class Surgeon which extends Doctor
 */
public class Surgeon extends Doctor {
    /**
     * Creates an instance of Surgeon which inheritances Doctor
     * @param firstname
     * @param surname
     * @param socialSecurityNumber
     */
    public Surgeon(String firstname, String surname, String socialSecurityNumber) {
        super(firstname, surname, socialSecurityNumber);
    }

    /**
     * Method to set a diagnosis on a patient
     * @param patient
     * @param diagnosis
     */
    @Override
    public void setDiagnosis(Patient patient, String diagnosis) {
        patient.setDiagnosis(diagnosis);
    }


}
