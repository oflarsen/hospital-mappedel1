package edu.ntnu.oflarsen.idatt2001.mappedel1.hospital.healthpersonal;

import edu.ntnu.oflarsen.idatt2001.mappedel1.hospital.Employee;

/**
 * Public class Nurse which extends Employee
 */
public class Nurse extends Employee {
    /**
     * Creates an instance of Nurse which inheritances Employee
     * @param firstname
     * @param surname
     * @param socialSecurityNumber
     */
    public Nurse(String firstname, String surname, String socialSecurityNumber) {
        super(firstname, surname, socialSecurityNumber);
    }

    /**
     * To string method
     * @return Surname and firstname in a string
     */
    @Override
    public String toString() {
        return "Nurse{" + super.getSurname() + ", " + super.getFirstname() +
        "}";
    }
}
