package edu.ntnu.oflarsen.idatt2001.mappedel1.hospital.exception;

/**
 * Public Exception class
 * This is an "checked" exception. This means that IntelliJ stops the program from running if this type of
 * exception is thrown, without being caught. If the exception wasn't checked, the program would run, and crash while running.
 * These types of exceptions are called errors.
 * Examples: OutOfMemoryError Exception
 */
public class RemoveException extends Exception {
    private static final long serialVersionUID = 1L;

    /**
     * Creates an instance of a RemoveException
     * @param errorMessage
     */
    public RemoveException(String errorMessage) {
        super(errorMessage);
    }
}
