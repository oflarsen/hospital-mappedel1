package edu.ntnu.oflarsen.idatt2001.mappedel1.hospital;

import java.util.Objects;

/**
 * Abstract class Person
 */
public abstract class Person {
    private String firstname;
    private String surname;
    private String socialSecurityNumber;

    /**
     * Creates an instance of Person
     * @param firstname
     * @param surname
     * @param socialSecurityNumber
     */
    public Person(String firstname, String surname, String socialSecurityNumber){
        this.firstname = firstname;
        this.surname = surname;
        this.socialSecurityNumber = socialSecurityNumber;
    }

    /**
     * Get method to get the firstname
     * @return
     */
    public String getFirstname() {
        return firstname;
    }

    /**
     * Set method to set a new firstname
     * @param firstname
     */
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    /**
     * Get method to get the surname
     * @return
     */
    public String getSurname() {
        return surname;
    }

    /**
     * Set method to set a new surname
     * @param surname
     */
    public void setSurname(String surname) {
        this.surname = surname;
    }

    /**
     * Get method to get the social security number
     * @return
     */
    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    /**
     * Set method to set the social security number
     * @param socialSecurityNumber
     */
    public void setSocialSecurityNumber(String socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
    }

    /**
     * To string method
     * @return String with firstname and surname
     */
    @Override
    public String toString() {
        return "Person{" +
                "firstname='" + firstname + '\'' +
                ", surname='" + surname + '\'' +
                //TODO KANSKJE IKKE I TOSTRING MED SOCIALSECURITY
                ", socialSecurityNumber='" + socialSecurityNumber + '\'' +
                '}';
    }

    /**
     * Equals which checks if two persons are the same based on firstname, surname and social security number
     * The social security number should be enough, but in the test data all the ssn are blank
     * I added this method even though it's not a part of the class diagram, because this is going to make it much
     * easier to check for duplicates in the add method in the Department class. The task doesn't tell us to check for duplicates,
     * but I think this makes the program more complex and better, therefore I will add this feature anyways.
     * This feature doesn't change the program, so that the client is unhappy, it will rather make it better
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Person)) return false;
        Person person = (Person) o;
        return firstname.equals(person.firstname) && surname.equals(person.surname) && Objects.equals(socialSecurityNumber, person.socialSecurityNumber);
    }

    /**
     * Hashcode for Person
     * @return
     */
    @Override
    public int hashCode() {
        return Objects.hash(firstname, surname, socialSecurityNumber);
    }
}
