package edu.ntnu.oflarsen.idatt2001.mappedel1.hospital;

import edu.ntnu.oflarsen.idatt2001.mappedel1.hospital.exception.RemoveException;

import java.util.ArrayList;
import java.util.Objects;

/**
 * Department class
 * A department has a list of employees working in the department, and a list of patients that are registered /
 * treated in that department
 */
public class Department {
    private String departmentName;
    private ArrayList<Employee> employees;
    private ArrayList<Patient> patients;

    /**
     * Creates an instance of a Department
     * @param departmentName
     */
    public Department(String departmentName) {
        this.departmentName = departmentName;
        employees = new ArrayList<>();
        patients = new ArrayList<>();
    }

    /**
     * Return the department name
     * @return
     */
    public String getDepartmentName() {
        return departmentName;
    }

    /**
     * Makes it possible to set a new department name
     * @param departmentName
     */
    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    /**
     * Return the an ArrayList of the employees working in the department
     * @return
     */
    public ArrayList<Employee> getEmployees() {
        return employees;
    }

    /**
     * A method to add new employees to the department
     * Checks if the employee is already registered using the equals method from the Person class
     * @param employee
     */
    public void addEmployee(Employee employee) {

        for (Employee e : employees){
            if (e.equals(employee) && employee.getClass() == e.getClass()){
                throw new IllegalArgumentException("Employee already registered");
            }

        }


        this.employees.add(employee);
        }

    /**
     * A method to add a new patient to the department
     * Uses the same method as in addEmployee to make sure the patient isn't already registered
     * @param patient
     */
    public void addPatient(Patient patient){
        if(patients.contains(patient)){
            throw new IllegalArgumentException("Patient already registered");
        }else {
            this.patients.add(patient);
        }
    }

    /**
     * Returns an ArrayList with all the patients in the department
     * @return
     */
    public ArrayList<Patient> getPatients(){
        return patients;
    }

    /**
     * A method to remove a person (Patient or Employee) from the department
     * Uses the equals method from Person to remove the Employee or Patient based on name and SocialSecurityNumber
     * @param person
     * @throws RemoveException If the person doesn't exist the method throws a RemoveException
     */
    public void remove(Person person) throws RemoveException {
        //This works because we added the equals method in Person, therefore this works,
        // and I don't have to loop through the whole list. Contains uses the the equals from the Person class to check through the ArrayList
        if (patients.contains(person)){

            patients.remove(person);

        }else if(employees.contains(person)) {

            employees.remove(person);
            // Added an extra remove, because in my program, you can have the same person with two different titles
            // Therefore, if you remove a person, I have to check of the person is still in the system with another work title
            if(employees.contains(person)){
                employees.remove(person);
            }

        }else {
            throw new RemoveException("The person you are trying to remove is not registered in the department!");
        }

    }

    /**
     * Equals method to check if two departments are the same
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Department)) return false;
        Department that = (Department) o;
        return departmentName.equals(that.departmentName) && Objects.equals(employees, that.employees) && Objects.equals(patients, that.patients);
    }

    /**
     * Hashcode
     * @return
     */
    @Override
    public int hashCode() {
        return Objects.hash(departmentName, employees, patients);
    }

    /**
     * To string method who returns a the department name
     * @return
     */
    @Override
    public String toString() {
        return "Department{" +
                departmentName + '\'' +
                '}';
    }
}
