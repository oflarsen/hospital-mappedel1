package edu.ntnu.oflarsen.idatt2001.mappedel1.hospital;

/**
 * Interface which is implemented in Patient
 *
 */
public interface Diagnosable {
    /**
     * Method to where a patient is set a diagnosis
     * @param diagnosis
     */
    void setDiagnosis(String diagnosis);
}
