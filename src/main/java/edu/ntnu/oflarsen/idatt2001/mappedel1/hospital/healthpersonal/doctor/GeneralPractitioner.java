package edu.ntnu.oflarsen.idatt2001.mappedel1.hospital.healthpersonal.doctor;

import edu.ntnu.oflarsen.idatt2001.mappedel1.hospital.Patient;
import edu.ntnu.oflarsen.idatt2001.mappedel1.hospital.healthpersonal.Doctor;

/**
 * Class General Practitioner which extends Doctor
 */
public class GeneralPractitioner extends Doctor {

    /**
     * Creates an instance of GeneralPractitioner which inheritances Doctor
     * @param firstname
     * @param surname
     * @param socialSecurityNumber
     */
    public GeneralPractitioner(String firstname, String surname, String socialSecurityNumber) {
        super(firstname, surname, socialSecurityNumber);
    }

    /**
     * Method to set diagnosis on a patient
     * @param patient
     * @param diagnosis
     */
    @Override
    public void setDiagnosis(Patient patient, String diagnosis) {
        patient.setDiagnosis(diagnosis);
    }

}
