package edu.ntnu.oflarsen.idatt2001.mappedel1.hospital;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * The class Hospital. This class consists of many departments, with employees and patients
 */
public class Hospital {
    private final String hospitalName;

    private HashMap<String, Department> departments;

    /**
     * Creates an instance of a Hospital
     * @param hospitalName
     */
    public Hospital(String hospitalName) {
        this.hospitalName = hospitalName;
        departments = new HashMap<>();
    }

    /**
     * Method to add a department to the Hospital
     * @param department
     */
    public void addDepartment(Department department){
        departments.put(department.getDepartmentName(),department);
    }

    /**
     * Method that returns all departments in the hospital in a HashMap
     * @return
     */
    public HashMap<String,Department> getDepartments() {
        return departments;
    }

    /**
     * Returns the hospitals name
     * @return
     */
    public String getHospitalName() {
        return hospitalName;
    }

    /**
     * To string method which returns the hospitals name and all departments
     * @return
     */
    @Override
    public String toString() {

        String str = "";

        for(Department d : departments.values()){
            str += d.toString() + "\n";
        }

        return "Hospital{" + "\n" +
                 hospitalName + '\n' +
                "Departments:" + "\n" + str +
                '}';
    }
}
