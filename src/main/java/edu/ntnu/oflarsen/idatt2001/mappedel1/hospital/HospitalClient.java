package edu.ntnu.oflarsen.idatt2001.mappedel1.hospital;

import edu.ntnu.oflarsen.idatt2001.mappedel1.hospital.exception.RemoveException;

public class HospitalClient {
    public static void main(String[] args) throws RemoveException {
        Hospital nlsh = new Hospital("Nordlandssykehuset");
        HospitalTestData.fillRegisterWithTestData(nlsh);

        for(Employee e: nlsh.getDepartments().get("Akutten").getEmployees()){
            System.out.println(e.toString());
        }

        // Using remove and removing existing employee
        for (Department d : nlsh.getDepartments().values()) {
            if (d.getDepartmentName().trim().equals("Akutten")) {
                d.remove(new Employee("Inco", "Gnito", ""));
            }
        }
        System.out.println("\nAfter removing Inco Gnito");
        for(Employee e: nlsh.getDepartments().get("Akutten").getEmployees()){
            System.out.println(e.toString());
        }

        System.out.println("\nRemoving patient that doesn't exist: ");
        // Using remove and not finding the patient and using try-catch to handle the error
        try {
            for (Department d : nlsh.getDepartments().values()) {
                if (d.getDepartmentName().equals("Akutten")) {
                    d.remove(new Patient("Ing", "Lykke", ""));
                }
            }
        }catch (RemoveException removeException){
            System.out.println(removeException.getMessage());
        }
    }
}
