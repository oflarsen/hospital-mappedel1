package edu.ntnu.oflarsen.idatt2001.mappedel1.hospital;

/**
 * Class Patient which extends Person and implements Diagnosable
 */
public class Patient extends Person implements Diagnosable{
    private String diagnosis = "";

    /**
     * Protected get method
     * @return diagnosis
     */
    protected String getDiagnosis(){
        return diagnosis;
    }

    /**
     * Creates an instance of Patient
     * @param firstname
     * @param surname
     * @param socialSecurityNumber
     */
    protected Patient(String firstname, String surname, String socialSecurityNumber) {
        super(firstname, surname, socialSecurityNumber);
    }

    /**
     * To string method which returns surname, firstname and diagnosis in a string
     * @return
     */
    @Override
    public String toString() {
        return "Patient{" + super.getSurname() + ", " + super.getFirstname() +
                ", diagnosis='" + diagnosis + '\'' +
                '}';
    }

    /**
     * Method implemented from Diagnosable interface
     * Makes it possible for a Doctor set a diagnosis on the patient
     * @param diagnosis
     */
    @Override
    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }
}
