package edu.ntnu.oflarsen.idatt2001.mappedel1.hospital.healthpersonal;

import edu.ntnu.oflarsen.idatt2001.mappedel1.hospital.Employee;
import edu.ntnu.oflarsen.idatt2001.mappedel1.hospital.Patient;

/**
 * Abstract class Doctor which extends Employee
 */
public abstract class Doctor extends Employee {
    /**
     * Creates an instance of Doctor, which inheritances Employee. Protected access
     * @param firstname
     * @param surname
     * @param socialSecurityNumber
     */
    protected Doctor(String firstname, String surname, String socialSecurityNumber) {
        super(firstname, surname, socialSecurityNumber);
    }

    /**
     * Abstract method to set a diagnosis on a patient
     * @param patient
     * @param diagnosis
     */
    public abstract void setDiagnosis(Patient patient, String diagnosis);

}
