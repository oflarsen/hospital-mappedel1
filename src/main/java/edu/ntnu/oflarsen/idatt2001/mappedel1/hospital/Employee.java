package edu.ntnu.oflarsen.idatt2001.mappedel1.hospital;

/**
 * Public class Employee which extends Person
 */
public class Employee extends Person {
    /**
     * Creates an instance of Employee which inheritances Person
     * @param firstname
     * @param surname
     * @param socialSecurityNumber
     */
    public Employee(String firstname, String surname, String socialSecurityNumber) {
        super(firstname, surname, socialSecurityNumber);
    }

    /**
     * To string method
     * @return surname and firstname in a String
     */
    @Override
    public String toString() {
        return "Employee{" + super.getSurname() + ", " + super.getFirstname() +
                "}";
    }
}
