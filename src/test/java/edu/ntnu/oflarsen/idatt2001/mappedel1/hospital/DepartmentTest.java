package edu.ntnu.oflarsen.idatt2001.mappedel1.hospital;

import edu.ntnu.oflarsen.idatt2001.mappedel1.hospital.exception.RemoveException;
import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

class DepartmentTest {

    Department emergency;
    Patient helge;
    Employee lars;
    @BeforeEach
    void setUp(){
        emergency = new Department("Emergency");
        helge = new Patient("Helge", "Larsen", "");
        lars = new Employee("Lars", "Hansen", "");
        emergency.addEmployee(lars);
        emergency.addPatient(helge);
    }

    @DisplayName("Testing the remove method with different tests")
    @Nested
    class remove {

        @Test
        void removePatientSuccessfully() throws RemoveException {
            assertTrue(emergency.getPatients().size() == 1);
            assertDoesNotThrow(() ->emergency.remove(helge));
            assertTrue(emergency.getPatients().size() == 0);
        }

        @Test
        void removeEmployeeSuccessfully() throws RemoveException {
            assertTrue(emergency.getEmployees().size() == 1);

            assertDoesNotThrow(() ->emergency.remove(lars));
            assertTrue(emergency.getEmployees().size() == 0);
        }

        @Test
        void removeExceptionThrownPatient() {
            RemoveException removeException = assertThrows(RemoveException.class, () -> {
                emergency.remove(new Patient("Adam", "Larsen", ""));
            });

            assertEquals("The person you are trying to remove is not registered in the department!", removeException.getMessage());
        }

        @Test
        void removeExceptionThrownEmployee() {
            RemoveException removeException = assertThrows(RemoveException.class, () -> {
                emergency.remove(new Employee("Adam", "Larsen", ""));
            });

            assertEquals("The person you are trying to remove is not registered in the department!", removeException.getMessage());
        }

        @Test
        void removeNull() {
            try {
                emergency.remove(null);
            } catch (NullPointerException | RemoveException ne) {
                assertTrue(true);
            }
        }
    }

    @DisplayName("Different tests to see if the equals method works")
    // This test is not needed, but added by me anyways, because I want to ensure that this method works
    // Mainly because this is a method added by me, and not requested by the client / in the class diagram
    @Nested
    class equals {

        @Test
        void employeesNotEqual() {
            assertFalse(lars.equals(new Employee("Lar", "Hans", "")));
        }

        @Test
        void employeeEqual() {
            assertTrue(lars.equals(new Employee("Lars", "Hansen", "")));
        }

        @Test
        void patientNotEqual() {
            assertFalse(helge.equals(new Patient("Helge", "Larse", "")));
        }

        @Test
        void patientEquals() {
            assertTrue(helge.equals(new Patient("Helge", "Larsen", "")));
        }

        @Test
        void patientNull() {
            try {
                emergency.addPatient(null);
            } catch (NullPointerException ne) {
                assertTrue(true);
            }
        }

        @Test
        void employeeNull() {
            try {
                emergency.addPatient(null);
            } catch (NullPointerException ne) {
                assertTrue(true);
            }
        }

    }
}